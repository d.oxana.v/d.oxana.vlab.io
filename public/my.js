window.addEventListener("DOMContentLoaded", function () {
    // Создаем константы
    const PRICE_1 = 100;
    const PRICE_2 = 111;
    const PRICE_3 = 123;
    const EXTRA_PRICE = 0;
    const RADIO_1 = 321;
    const RADIO_2 = 123;
    const RADIO_3 = 111;
    const RADIO_4 = 333;
    const CHECKBOX = 55;
    // Создаем переменные
    let price = PRICE_1;
    let extraPrice = EXTRA_PRICE;
    let result;
    // Создаем html элементы
    let calc = document.getElementById("calc");
    let myNum = document.getElementById("number");
    let resultSpan = document.getElementById("result-span");
    let select = document.getElementById("select");
    let radiosDiv = document.getElementById("radio-div");
    let radios = document.querySelectorAll("#radio-div input[type=radio]");
    let checkboxDiv = document.getElementById("check-div");
    let checkbox = document.getElementById("checkbox");
    // Функция обработки изменений в select
    select.addEventListener("change", function (event) {
        // Выбранный элемент option
        let option = event.target;
        // Смотрим какой выбран и отображаем элементы
        if (option.value === "1") {
            // Включаем радиокнопки и выключаем чекбокс
            radiosDiv.classList.add("d-none");
            checkboxDiv.classList.add("d-none");
            // Изменяем доп. цену
            extraPrice = EXTRA_PRICE;
            // Изменяем цену за штуку
            price = PRICE_1;
        }
        if (option.value === "2") {
            radiosDiv.classList.remove("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = RADIO_1;
            price = PRICE_2;
            // Сбрасываем радиокнопку
            document.getElementById("radio1").checked = true;
        }
        if (option.value === "3") {
            checkboxDiv.classList.remove("d-none");
            radiosDiv.classList.add("d-none");
            extraPrice = EXTRA_PRICE;
            price = PRICE_3;
            // Сбрасываем чекбокс
            checkbox.checked = false;
        }
    });
    // Обрабатываем нажатия на радиокнопки
    radios.forEach(function (currentRadio) {
        currentRadio.addEventListener("change", function (event) {
            // Выбранная радиокнопка
            let radio = event.target;
            // Узнаем конкретную радиокнопку
            if (radio.value === "r1") {
                // Изменяем доп. цену
                extraPrice = RADIO_1;
            }
            if (radio.value === "r2") {
                extraPrice = RADIO_2;
            }
            if (radio.value === "r3") {
                extraPrice = RADIO_3;
            }
            if (radio.value === "r4") {
                extraPrice = RADIO_4;
            }
        });
    });
    // Обрабатываем нажатие чекбокса
    checkbox.addEventListener("change", function () {
        // Если нажат, меняем доп. цену, иначе сбрасываем
        if (checkbox.checked) {
            extraPrice = CHECKBOX;
        } else {
            extraPrice = EXTRA_PRICE;
        }
    });
    // Обрабатываем любые изменения в калькуляторе
    // Если они есть, пересчитываем итоговую цену
    calc.addEventListener("change", function () {
        // Защита от отрицательного количества
        if (myNum.value < 1) {
            myNum.value = 1;
        }
        // Пересчет результата
        result = (price + extraPrice) * myNum.value;
        // Вставка результата в HTML
        resultSpan.innerHTML = result;
    });
});